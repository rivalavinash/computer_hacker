﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour
{
    const string menuHint = "type 'menu' for Main Menu";

    string[] level1Passwords = { "book", "aisle", "shelf", "password", "font", "borrow", "resource", "author", "publisher", "subject", "catalogue", "fiction", "biography", "available", "due" };

    string[] level2Passwords = { "Prisoner","handcuff","holster","uniform","arrest","police","bail","burglar","cell","criminal","cruiser","custody",
    "detective","escape","felony","fingerprints","guilty","innocent","intruder","patrol","perpetrator","pickpocket","pistol","pursuit","radar" };

    string[] level3Passwords = { "starfield","telescope","environment","exploration","astronaut","rocket","inspection","diagnosis","checkup","check","inquisition","inquiry",
    "inquest","exploration","disquisition","delving","reinvestigation" };

    //---------------------------Friends Forever-------------------------------
    //-------------------------ankit-------------------------------------------
    string[] ankitmotivation ={"'Believe you can and you’re halfway there'","'You have to expect things of yourself before you can do them'","It always seems impossible until it’s done",
    "Don’t let what you cannot do interfere with what you can do","Start where you are. Use what you have. Do what you can.","The secret of success is to do the common things uncommonly well.",
    "trive for progress, not perfection."," I find that the harder I work, the more luck I seem to have.","Don’t wish it were easier; wish you were better."};
    //------------------------rajan-------------------------------------------------
    string[] rajanmotivation = {"There are no shortcuts to any place worth going","Push yourself, because no one else is going to do it for you.",
    "There is no substitute for hard work.","The difference between ordinary and extraordinary is that little extra.","There are no traffic jams on the extra mile",
    " It’s not about how bad you want it. It’s about how hard you’re willing to work for it.","The difference between ordinary and extraordinary is that little “extra.”",};
    //------------------------bobby-------------------------------------------------
    string[] bobbymotivation = {"I don't need to 'get a life'. I'm a gamer. I have lots of lives.","Gamers don't die, they respawn.","There are millions of sci-fi enthusiasts in the world, not just gamers.",
    "Gamers always believe that an epic win is possible and that it's always worth trying and trying now.","We do not stop playing because we grow old. We grow old because we stop playing.",
    "It is in games that many men discover their paradise.","For a game, you don't need a teacher.","Games are all about taking risks."};
    //---------------------avinash---------------------------------------------------------
    string[] avinashmotivation = {"The quieter you become, the more you are able to hear...","Never tell everything you know...","It’s less about technology for me, and more about religion.",
    "Younger hackers are hard to classify.They're probably just as diverse as the old hackers are.We're all over the map."};
    //------------------------kundan------------------------------------------------------
    string[] kundanmotivation ={"Never stop being a good person because of bad people.","A peaceful heart is a reward alone for being a good person.","Be a good person. But don’t waste time proving it.",
    "You can be a good person with a kind heart and still say no.","I define a good person as somebody who is fully conscious of their own limitations."};
    //-----------------------Friends Forever End-------------------------------- 
    //--------------------------------------------------------------------------
    
    int level;
    string password;
    enum Screen { MainMenu, Password, Win };
    Screen currentScreen;

    //--------------------------------------------------------------------------

    // Start is called before the first frame update
    void Start()
    {
        ShowMainMenu();
    }

    private static void ShowMainMenu()
    {  
        Terminal.WriteLine("What do you want to hack into ?");
        Terminal.WriteLine("Press 1 for School Library");
        Terminal.WriteLine("Press 2 for Police Station");
        Terminal.WriteLine("Press 3 for ISRO");
        Terminal.WriteLine("Type quite or close or exit for exit");
        Terminal.WriteLine("Type credit for our details");
        Terminal.WriteLine(" \n Enter your Selection :-");
    }

    //-----------------------------------------------------------------------
    void OnUserInput(string input)
    {
        if (input == "menu")
        {
            ShowMainMenu();
        }
        else if (input == "quit" || input == "close" || input == "exit")
        {
            Application.Quit();
        }
        else if (currentScreen == Screen.MainMenu)
        {
            RunMainMenu(input);
        }
        else if (currentScreen == Screen.Password)
        {
            CheckPassword(input);
        }
    }
    //-----------------------------------------------------------------
    void RunMainMenu(string input)
    {
        bool isValidLevelNumber = (input == "1" || input == "2" || input == "3");
        if (isValidLevelNumber)
        {
            level = int.Parse(input);
            AskForPassword();
        }
        else if (input == "avinash")
        {
            var avi = Random.Range(0, avinashmotivation.Length);
            Terminal.ClearScreen();
            Terminal.WriteLine(menuHint);
            Terminal.WriteLine("\n");
            Terminal.WriteLine(avinashmotivation[avi]);
            Terminal.WriteLine("\n\t\t\t\tMr.Avinash\n");
        }
        //----------------------------------Friends Forever-------------------------------------        
        else if (input == "bobby")
        {
            var singh = Random.Range(0, bobbymotivation.Length);
            Terminal.ClearScreen();
            Terminal.WriteLine(menuHint);
            Terminal.WriteLine("\n");
            Terminal.WriteLine(bobbymotivation[singh]);
            Terminal.WriteLine("\n\t\t\t\tMr.Bobby\n");
        }
        else if (input == "ankit")
        {
            var ank = Random.Range(0, ankitmotivation.Length);
            Terminal.ClearScreen();
            Terminal.WriteLine(menuHint);
            Terminal.WriteLine("\n");
            Terminal.WriteLine(ankitmotivation[ank]);
            Terminal.WriteLine("\n\t\t\t\tMr.Ankit\n");
        }
        else if (input == "rajan")
        {
            var raj = Random.Range(0, rajanmotivation.Length);
            Terminal.ClearScreen();
            Terminal.WriteLine(menuHint);
            Terminal.WriteLine("\n");
            Terminal.WriteLine(rajanmotivation[raj]);
            Terminal.WriteLine("\n\t\t\t\tMr.Rajan\n");
        }
        else if (input == "kundan")
        {
            var kdn = Random.Range(0, kundanmotivation.Length);
            Terminal.ClearScreen();
            Terminal.WriteLine(menuHint);
            Terminal.WriteLine("\n");
            Terminal.WriteLine(kundanmotivation[kdn]);
            Terminal.WriteLine("\n\t\t\t\tMr.Kundan\n");
        }
        else if (input == "credit")
        {
            Terminal.ClearScreen();
            Terminal.WriteLine(menuHint);
            Terminal.WriteLine("\n\tDesign by Avenio Gaming\n");
            Terminal.WriteLine("  subscribe our Youtube Channel");
            Terminal.WriteLine("\t\tavenio gaming\n");
            Terminal.WriteLine("\t\t-:Follow us on:-");
            Terminal.WriteLine("\tTwitter:-@aveniogaming");
            Terminal.WriteLine("\tIsntagram:-@aveniogaming");
            Terminal.WriteLine("\tFacebook:-@aveniogaming");
        }
        //-------------------------------Friends Forever section end----------------------------               
        else
        {
            Terminal.WriteLine("Please Choose a valid level");
            Terminal.WriteLine(menuHint);
        }
    }
    //-------------------------------------------------------------------   
    void AskForPassword()
    {
        currentScreen = Screen.Password;
        Terminal.ClearScreen();
        SetRandomPassword();
        Terminal.WriteLine("Enter your password , hint: " + password.Anagram());
        Terminal.WriteLine(menuHint);
    }
    //--------------------------------------------------------------------    
    void SetRandomPassword()
    {
        switch (level)
        {
            case 1:
                password = level1Passwords[Random.Range(0, level1Passwords.Length)];
                break;
            case 2:
                password = level2Passwords[Random.Range(0, level2Passwords.Length)];
                break;
            case 3:
                password = level3Passwords[Random.Range(0, level3Passwords.Length)];
                break;
            default:
                Debug.LogError("Invalid level number");
                break;
        }
    }
    //--------------------------------------------------------------------
    void CheckPassword(string input)
    {
        if (input == password)
        {
            DisplayWinScreen();
        }
        else
        {
            AskForPassword();
        }
    }
    //--------------------------------------------------------------------
    void DisplayWinScreen()
    {
        currentScreen = Screen.Win;
        Terminal.ClearScreen();
        ShowLevelReward();
    }
    //--------------------------------------------------------------------
    void ShowLevelReward()
    {
        switch (level)
        {
            case 1:
                Terminal.WriteLine("Have a book...");
                Terminal.WriteLine(@"
       __________
      /         /
     / AVINASH /
    /_________/   
   (_________/
");
                Terminal.WriteLine(menuHint);
                break;
            case 2:
                Terminal.WriteLine("You got the prison key");
                Terminal.WriteLine(@"
   __
  /0 \______
  \__/-='='

");
                Terminal.WriteLine(menuHint);
                break;
            case 3:
                Terminal.WriteLine("Play again for a greater challenge");
                Terminal.WriteLine("You got the advance robot");
                Terminal.WriteLine(@"
  __________
  | 0    0 |
  |   \/   |
  |   ==   |
  |________|
");
                Terminal.WriteLine(menuHint);
                break;
            default:
                Debug.LogError("Invalid Level reached");
                break;
        }
    }
    //--------------------------------------------------------------------    
}
